\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{opndocument}[2020/11/27 OPNDev document class]

\DeclareOption{dut}{\PassOptionsToPackage{dutch}{babe}}
\DeclareOption{eng}{\PassOptionsToPackage{english}{babel}}

%\DeclareOption{draft}{\drafttrue}
\ExecuteOptions{dut}
\ProcessOptions\relax

%\if\@drafttrue
%  \RequirePackage{draftwatermark}
%  \SetWatermarkText{DRAFT}
%  \SetWatermarkScale{1}
%\fi

\LoadClass[a4paper,10pt]{article}

\RequirePackage[gen]{eurosym}
\RequirePackage{comment}
\RequirePackage{fancyhdr}
\RequirePackage{hyperref}
\RequirePackage{enumitem}

\RequirePackage{geometry}
\geometry{a4paper,tmargin=2.5cm,bmargin=2.5cm,lmargin=2.5cm,rmargin=2.5cm}

\RequirePackage[T1]{fontenc}
\RequirePackage[default]{raleway}

\pagestyle{fancyplain}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}

\newcommand{\opnBaseHeader}[3]{
  \begin{center}
    {\Large #1 \\ #2}\\
    \noindent
    #3\\
    \hrule height .1mm
    \vspace{.5cm}
  \end{center}
}

\newcommand{\opnDocumentFooter}[3]{
  \lfoot{#1}
  \cfoot{#2}
  \rfoot{#3}
}
